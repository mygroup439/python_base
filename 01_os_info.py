import sys
import platform

info = 'OS info is {}\nPython version is {} {}'.format(platform.system(), sys.version, platform.architecture())
print(info)

with open('os_info.txt', 'w') as ff:
    ff.write(info)